@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 mb-4">
                <h1 class="h3 mb-0 text-gray-800">Products Listing</h1>
            </div>
            <div class="col-md-7 mb-4">
            </div>
            <div class="col-md-1 mb-4">
                <a href="{{ route('product.create') }}" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Create</a>
            </div>
        </div>
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12 mb-4">
            @if(session()->has('message'))
                <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
            <table class="table table-striped">
                <thead>
                    <tr>
					<td>ID</td>
					<td>Photo</td>
                    <td>Name</td>
					<td>Quantity</td>
					<td>Category</td>
					<td colspan="2">Action</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($products) && $products->count())
                        @foreach($products as $product)
                            <tr>
								<td style="width: 10%;">{{$loop->iteration}}</td>
								<td style="width: 30%;">
                                    <a href="{{ asset('/images/'.$product->photo) }}" target="_blank"><img src="{{ asset('/images/'.$product->photo) }}" alt="{{$product->photo}}" height="50px"></a>
                                </td>
                                <td style="width: 20%;">{{$product->name}}</td>
								<td style="width: 20%;">{{$product->qty}}</td>
								<td style="width: 20%;">{{$product->category_slug['name']}}</td>
                                <td style="width: 20%;">
                                    <a href="{{ URL::to('product/' . $product->id . '/edit') }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ URL::to('product/' . $product->id. '/delete') }}" method="post" style="display: inline;">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><center>No Data Available </center></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <!-- /.container-fluid -->
@endsection

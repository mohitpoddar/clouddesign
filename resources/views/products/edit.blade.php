@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Product</h1>
            <a href="{{URL::previous()}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Back</a>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12 mb-4">
                @if ( count( $errors ) > 0 )
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">{{ session()->get('message') }}</div>
                @endif
                <form method="post" action="{{ route('product.update',$product->id) }}" name="form" enctype="multipart/form-data">
                    @method('PATCH')
                    @include('products.form')
                </form>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection

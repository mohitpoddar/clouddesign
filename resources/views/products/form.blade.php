<div class="card-body">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name" value="{{($product->name) ?? old('name')}}" class="form-control form-control-sm" required/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Quantity</label>
                <input type="number" name="qty" placeholder="Quantity" value="{{($product->qty)?? old('qty')}}" class="form-control form-control-sm" required/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Photo</label><br>
                <input type="file" name="photo"/>
                @if(isset($product->photo))
                <a href="{{ url('storage/') }}/{{$product->photo}}" target="_blank">{{$product->photo}}</a>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Category Name</label>
                <select class="form-control form-control-sm" name="category">
                    @foreach($categories as $category)
                        <option {{(isset($product->category) && ($category->slug == $product->category) )? 'selected' : old('category')}} value="{{$category->slug}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<div class="card-footer text-right mt-3">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
</div>

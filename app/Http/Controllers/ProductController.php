<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#libraries
use Session;
use Illuminate\Support\Facades\Validator;
#Models
use App\Models\Product;
use App\Models\Category;
#Repository
use App\Repositories\ProductRepository;

class ProductController extends Controller 
{
    /**
	 * @var $product
	 */
	private $product; 
 
	/**
	 * productController constructor.
	 *
	 * @param App\Repositories\ProductRepository $product
	 */
	public function __construct(ProductRepository $product) 
	{
		$this->product = $product;
	}
 
	/**
	 * Get all products.
	 *
	 * @return Illuminate\View
	 */
	public function getAllproducts($id = null)
	{
		$products = $this->product->getAll();
		$editproduct = (isset($id)) ? $this->product->getById($id) : null;
		return view('products.index', compact('products', 'editproduct'));
	}
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function postCreateProduct()
	{	
		$categories = Category::all();
		return view('products.create',compact('categories'));
	}
	/**
	 * Store a product
	 *
	 * @var array $attributes
	 *
	 * @return mixed
	 */
	public function postStoreproduct(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'name'        => 'required',
            'qty'         => 'required'
        ]);
        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();
            // redirect our user back to the form with the errors from the validator
            return back()->withInput()->withErrors($validator);
		}
		$attributes = $request->all();
		//upload image
        if($request->hasFile('photo')) {
			$request->validate([
				'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);
			$imageName = time().'.'.$request->photo->extension();  
			$request->photo->move(public_path('images'), $imageName);
            $attributes['photo'] = $imageName;
		}
		//create product
		$this->product->create($attributes);
		Session::flash('message', 'Successfully Created the Product!');
		return redirect()->route('product.index');
	}
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function postEditProduct($id)
	{	
		$product = Product::with('category_slug')->where('id',$id)->first();
        if (!$product) {
            abort(401);
        }
		$categories = Category::all();
		return view('products.edit',compact('categories','product'));
	}
	/**
	 * Update a product
	 *
	 * @var integer $id
	 * @var array 	$attributes
	 *
	 * @return mixed
	 */
	public function postUpdateproduct($id, Request $request)
	{
		$attributes = $request->all();
		//upload image
        if($request->hasFile('photo')) {
			$imageName = time().'.'.$request->photo->extension();  
			$request->photo->move(public_path('images'), $imageName);
            $attributes['photo'] = $imageName;
		}
		//update data
		$this->product->update($id, $attributes);
		Session::flash('message', 'Successfully Updated the Product!');
		return redirect()->route('product.index');
	}
 
	/**
	 * Delete a product
	 *
	 * @var integer $id
	 *
	 * @return mixed
	 */
	public function postDeleteproduct($id)
	{	
		$this->product->delete($id);
		Session::flash('message', 'Successfully deleted the Product!');
		return redirect()->route('product.index');
	}
}

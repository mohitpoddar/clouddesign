<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','photo', 'qty', 'category'];

    /**
    * Get the related category
    */
    public function category_slug()
    {
        return $this->belongsTo('App\Models\Category','category','slug');
    }
}

<?php

namespace App\Repositories;

use App\Models\Product;

class EloquentProduct implements ProductRepository
{
	/**
	 * @var $model
	 */
	private $model;

	/**
	 * EloquentProduct constructor.
	 *
	 * @param App\Product $model
	 */
	public function __construct(Product $model)
	{
		$this->model = $model;
	}

	/**
	 * Get all products.
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAll()
	{
		return $this->model->with('category_slug')->get();
	}

	/**
	 * Get product by id.
	 *
	 * @param integer $id
	 *
	 * @return App\Product
	 */
	public function getById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Create a new products.
	 *
	 * @param array $attributes
	 *
	 * @return App\Product
	 */
	public function create(array $attributes)
	{
		return $this->model->create($attributes);
	}

	/**
	 * Update a Product.
	 *
	 * @param integer $id
	 * @param array $attributes
	 *
	 * @return App\Product
	 */
	public function update($id, array $attributes)
	{
		return $this->model->find($id)->update($attributes);
	}

	/**
	 * Delete a Product.
	 *
	 * @param integer $id
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		return $this->model->find($id)->delete();
	}
}
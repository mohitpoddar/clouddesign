<?php

namespace App\Repositories;

interface ProductRepository
{
	function getAll();

	function getById($id);

	function create(array $attributes);

	function update($id, array $attributes);

	function delete($id);
}
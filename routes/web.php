<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\ProductController::class, 'getAllProducts'])->name('product.index');
Route::get('/products', [App\Http\Controllers\ProductController::class, 'getAllProducts'])->name('product.index');
Route::get('/product/create', [App\Http\Controllers\ProductController::class, 'postCreateProduct'])->name('product.create');
Route::post('/product/store', [App\Http\Controllers\ProductController::class, 'postStoreProduct'])->name('product.store');
Route::get('/product/{id}/edit', [App\Http\Controllers\ProductController::class, 'postEditProduct'])->name('product.edit');
Route::patch('/product/{id}/update', [App\Http\Controllers\ProductController::class, 'postUpdateProduct'])->name('product.update');
Route::delete('/product/{id}/delete', [App\Http\Controllers\ProductController::class, 'postDeleteProduct'])->name('product.delete');
